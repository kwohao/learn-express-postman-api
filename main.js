// bagian route yang tidak diproses dalam contorller/model

const express = require("express");
const app = express();
const userRouter = require("./users/route");
const path = require("path");

app.use(express.json());
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.sendFile("index.html");
});

app.use("/", userRouter);

app.listen(3000, () => {
  console.log(`Example app listening on port 3000`);
});
