// bagian route atau pemanggilan post/get yang sudah di proses dalam controller/model

const express = require("express");
const userRouter = express.Router();
const userController = require("./controller");

userRouter.get("/users", userController.getAllUsers);
userRouter.post("/regis", userController.registerNewUser);
userRouter.post("/regisbio", userController.registerNewUserBio);
userRouter.post("/login", userController.loginUser);
userRouter.get("/findbio/:userId", userController.findUserBio);
userRouter.put("/updatebio/:userId", userController.updateUserBio);
userRouter.put("/inputhistory", userController.inputHistory);
userRouter.get("/gamehistory/:userId", userController.gameHistory);

module.exports = userRouter;
