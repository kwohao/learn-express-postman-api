// model = bagian terima data

const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

let userList = [];

class UserModel {
  // cek user data ganti userList
  getAllUsers = async () => {
    return await db.users.findAll({ include: [db.user_bio, db.user_history] });
  };

  findUserBio = async (userId) => {
    // return await db.user_bio.findOne({ where: { id: userId } });
    return await db.users.findOne({
      include: [db.user_bio],
      where: { id: userId },
    });
  };

  isUserBioExist = async (userBio) => {
    const bioExist = await db.user_bio.findOne({
      where: {
        [Op.or]: [
          { fullname: userBio.fullname },
          { phone: userBio.phone },
          { user_id: userBio.user_id },
        ],
      },
    });

    if (bioExist) {
      return true;
    } else {
      return false;
    }
  };

  //  cek user existence ganti + awa asy
  isUserExist = async (userData) => {
    const alrdExist = await db.users.findOne({
      where: {
        [Op.or]: [{ username: userData.username }, { email: userData.email }],
      },
    });

    if (alrdExist) {
      return true;
    } else {
      return false;
    }
  };

  registerNewUserBio = (userBio) => {
    db.user_bio.create({
      fullname: userBio.fullname,
      address: userBio.address,
      phone: userBio.phone,
      Age: userBio.Age,
      user_id: userBio.user_id,
    });
  };

  //    method register ganti userData user,ema,pass
  registerNewUser = (userData) => {
    db.users.create({
      username: userData.username,
      email: userData.email,
      password: md5(userData.password),
    });
  };

  updateUserBio = async (userId, fullname, address, phone) => {
    return await db.user_bio.update(
      { fullname: fullname, address: address, phone: phone },
      { where: { user_id: userId } }
    );
  };

  inputHistory = (userGame) => {
    db.user_history.create({
      games: userGame.games,
      status: userGame.status,
      time: userGame.time,
      user_id: userGame.user_id,
    });
  };

  gameHistory = async (userId) => {
    // return await db.user_history.findOne({ where: { id: userId } });
    return await db.users.findOne({
      include: [db.user_history],
      where: { id: userId },
    });
  };

  // method login
  userLogin = async (username, email, password) => {
    const sortedUser = await db.users.findOne({
      where: { username: username, password: md5(password) },
    });
    return sortedUser;
  };
}

module.exports = new UserModel();
