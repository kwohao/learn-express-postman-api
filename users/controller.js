// bagian semua function, logic, atau business

const userModels = require("./model");

class UserController {
  getAllUsers = async (req, res) => {
    const allUsers = await userModels.getAllUsers();
    return res.json(allUsers);
  };

  findUserBio = async (req, res) => {
    const { userId } = req.params;
    console.log(userId);

    try {
      const user = await userModels.findUserBio(userId);

      if (user) {
        return res.json(user);
      } else {
        res.statusCode = 400;
        return res.json({ message: "user id tidak ditemukan: " + userId });
      }
    } catch (error) {
      res.statusCode = 400;
      console.log(userId);
      return res.json({ message: "user id tidak ditemukan: " + userId });
    }
  };

  registerNewUserBio = async (req, res) => {
    const userBio = req.body;
    try {
      if (
        userBio.fullname == "" &&
        userBio.address == "" &&
        userBio.phone == "" &&
        userBio.Age == "" &&
        userBio.user_id == ""
      ) {
        res.statusCode = 400;
        return res.json({ message: "Tolong isi data diri dengan lengkap" });
      } else if (userBio.address == "") {
        res.statusCode = 400;
        return res.json({ message: "Tolong masukan alamat kamu" });
      } else if (userBio.phone == "") {
        res.statusCode = 400;
        return res.json({ message: "Tolong masukan no.hp kamu" });
      } else if (userBio.fullname == "") {
        res.statusCode = 400;
        return res.json({ message: "Tolong masukan nama lengkap kamu" });
      } else if (userBio.Age == "") {
        res.statusCode = 400;
        return res.json({ message: "Tolong masukan usia kamu" });
      } else if (userBio.user_id == "") {
        res.statusCode = 400;
        return res.json({
          message: "Tolong masukan user id kamu yang telah terdaftar",
        });
      }
    } catch (error) {
      console.log(error);
    }

    //  ganti user,ema,pass userBio + await
    const bioExist = await userModels.isUserBioExist(userBio);
    if (bioExist) {
      res.statusCode = 400;
      return res.json({ message: "Some data already exist" });
    }
    // params ganti userBio
    userModels.registerNewUserBio(userBio);
    return res.json({ message: "Sukses menambahkan user baru!" });
  };

  registerNewUser = async (req, res) => {
    const userData = req.body;

    if (
      userData.username == "" &&
      userData.password == "" &&
      userData.email == ""
    ) {
      res.statusCode = 400;
      return res.json({ message: "Tolong isi data diri dengan lengkap" });
    } else if (userData.password == "") {
      res.statusCode = 400;
      return res.json({ message: "Tolong masukan password kamu" });
    } else if (userData.email == "") {
      res.statusCode = 400;
      return res.json({ message: "Tolong masukan email kamu" });
    } else if (userData.username == "") {
      res.statusCode = 400;
      return res.json({ message: "Tolong masukan username kamu" });
    }

    const alrdExist = await userModels.isUserExist(userData);
    if (alrdExist) {
      res.statusCode = 400;
      return res.json({ message: "Username or Email already exist" });
    }
    // params ganti userData
    userModels.registerNewUser(userData);
    return res.json({ message: "Sukses menambahkan user baru!" });
  };

  loginUser = async (req, res) => {
    const { username, email, password } = req.body;
    const loginData = await userModels.userLogin(username, email, password);

    if (loginData) {
      return res.json(loginData);
    } else {
      res.statusCode = 404;
      return res.json({ message: "Credential Tidak ditemukan" });
    }
  };

  updateUserBio = async (req, res) => {
    const { userId } = req.params;
    const { fullname, phone, address } = req.body;
    const UserBiodata = await userModels.updateUserBio(
      userId,
      fullname,
      address,
      phone
    );
    return res.json(UserBiodata);
  };

  inputHistory = async (req, res) => {
    const userGame = req.body;

    if (
      userGame.games == "" &&
      userGame.status == "" &&
      userGame.user_id == ""
    ) {
      return res.json({ message: "Tolong isi data diri dengan lengkap" });
    } else if (userGame.status == "") {
      return res.json({ message: "Tolong masukan status kamu" });
    } else if (userGame.user_id == "") {
      return res.json({ message: "Tolong masukan user id kamu" });
    } else if (userGame.games == "") {
      return res.json({ message: "Tolong masukan nama games kamu" });
    }

    userModels.inputHistory(userGame);
    return res.json({ message: "sukses menambahkan user history" });
  };

  gameHistory = async (req, res) => {
    const { userId } = req.params;

    const gameHistory = await userModels.gameHistory(userId);
    return res.json(gameHistory);
  };
}

module.exports = new UserController();
